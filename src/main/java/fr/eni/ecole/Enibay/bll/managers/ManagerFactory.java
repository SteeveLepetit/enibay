package fr.eni.ecole.Enibay.bll.managers;

public class ManagerFactory {

	public static UtilisateurManager getUtilisateurManager() {
		return UtilisateurManagerImpl.getInstance();
	}
	
	public static ArticleManager getArticleManager() {
		return ArticleManagerImpl.getInstance();
	}
	
	public static EnchereManager getEnchereManager() {
		return EnchereManagerImpl.getInstance();
	}
}
