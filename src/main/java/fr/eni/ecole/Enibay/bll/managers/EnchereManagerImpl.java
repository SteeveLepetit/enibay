package fr.eni.ecole.Enibay.bll.managers;

import java.time.LocalDateTime;
import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.dal.dao.DAOFactory;
import fr.eni.ecole.Enibay.dal.dao.EnchereDAO;

public class EnchereManagerImpl implements EnchereManager {
	private ArticleManager articleManager = ManagerFactory.getArticleManager();
	private static EnchereManager instance = null;

	public static EnchereManager getInstance() {
		if (instance == null) {
			instance = new EnchereManagerImpl();
		}
		return instance;
	}

	private EnchereManagerImpl() {
	}
	
	private EnchereDAO enchereDao = DAOFactory.getEnchereDAO();

	@Override
	public Enchere saveEnchere(Enchere enchere) {
		enchere.setDate(LocalDateTime.now());
		return enchereDao.saveEnchere(enchere);
	}
	
	public int calculNewPrixVente(Enchere enchere) {
		int newPrixVente = enchere.getArticle().getPrixVente();
		newPrixVente = enchere.getMontantEnchere();
		return newPrixVente;

	}
	
	@Override
	public Enchere findEnchereByIdArticle(int noArticle) {
		return enchereDao.findEnchereByIdArticle(noArticle);
	}

	@Override
	public Enchere findAntepenultiemeEnchereByArticle(int numArticle) {
		return enchereDao.findAntepenultiemeEnchereByArticle(numArticle);
	}

	





}
