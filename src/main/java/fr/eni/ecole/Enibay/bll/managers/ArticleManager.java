package fr.eni.ecole.Enibay.bll.managers;

import java.util.List;
import java.util.Map;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface ArticleManager {

	//INITIALISER save
	Article saveArticle(Article article);
	//VERIFICATION erreurs de saisie lors de la cr�ation
	Map<String, String> verifArticle(Article article);
	//LISTE tous les articles
	List<Article> findAllArticle();
	//LISTE les articles par categorie
	public List<Article> findArticleByCategorie(String categorie);
	//FIND by id
	Article findByNoArticle(int noArticle);
	//UPDATE
	void updateEtatVente(Article article);
	//UPDATE LE PRIX DE VENTE QUAND QUELQU'UN ENCHERIT
	void updatePrixVente(Article article, int newPrixVente);
	//LISTE barre de recherche 
	List<Article> findByNameArticle(String recherche);
	//LISTE article checkbox
	List<Article> findArticleOuvertes(Utilisateur utilisateurConnecte, List<String> lstEtatCheckbox);
	
	
	


	
}
