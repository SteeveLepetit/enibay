package fr.eni.ecole.Enibay.bll.managers;


import java.util.Map;

import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;



	
	

import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface UtilisateurManager  {

	public Map<String, String> verifInscription(Utilisateur u, String mdpConf);
	
	//Inscription save
	Utilisateur saveUtilisateur(Utilisateur utilisateur);
	//List tous les utilisateurs
	List<Utilisateur> findAllUtilisateur();
	//Connexion
	Utilisateur findByMailOrPseudoAndMdp(String idConnexion, String mdp);
	//Supprime par id 
	boolean deleteByIdUtilisateur(int noUtilisateur);
	//Update 
	boolean updateUtilisateur(Utilisateur utilisateur, String NewMdp);
	//Mot de passe oublie
	Utilisateur updateForgottenPassword(String email,String newMdp);
	//Find by pseudo
	Utilisateur findByPseudo(String pseudo);
	//Modification credit 
	boolean creditMaj(Utilisateur utilisateur, int montantEnchere);
	//Recreditation credit
	boolean recreditation(Utilisateur utilisateur, int montantEnchere);
	
	
}
