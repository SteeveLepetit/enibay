package fr.eni.ecole.Enibay.bll.bo;

public enum Categorie {
	AMEUBLEMENT(1), INFORMATIQUE(2), VETEMENT(3), SPORTLOISIRS(4);
	
	private int noCategorie;
	
	private Categorie(int noCategorie) {
		this.noCategorie = noCategorie;
	}

	public int getNoCategorie() {
		return noCategorie;
	}
	
	
	
}
