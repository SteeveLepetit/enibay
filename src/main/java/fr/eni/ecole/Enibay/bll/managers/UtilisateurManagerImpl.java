package fr.eni.ecole.Enibay.bll.managers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.dal.dao.DAOFactory;
import fr.eni.ecole.Enibay.dal.dao.UtilisateurDAO;

public class UtilisateurManagerImpl implements UtilisateurManager {

	private static UtilisateurManager instance = null;

	public static UtilisateurManager getInstance() {
		if (instance == null) {
			instance = new UtilisateurManagerImpl();
		}
		return instance;
	}

	private UtilisateurManagerImpl() {

	}

	private UtilisateurDAO utilisateurDao = DAOFactory.getUtilisateurDAO();

	@Override
	public Map<String, String> verifInscription(Utilisateur u, String mdpConf) {
		Map<String, String> erreurs = new HashMap<>();
		String pseudo = u.getPseudo();
		erreurs.clear();

		if (!u.getMdp().equals(mdpConf)) {
			erreurs.put("mdp", "Les mots de passe de coincident pas");
		}

		if (ChronoUnit.YEARS.between(u.getDdn(), LocalDate.now()) < 13) {
			erreurs.put("date", "Les inscriptions sont possibles � partir de 13 ans");
		}
		if (pseudo != null && pseudo.matches("[A-Za-z0-9]+")) {
			System.out.println();
		} else {
			erreurs.put("pseudoIncorrect", "Le pseudo doit contenir que des caracteres alphanumeriques ");
		}

		return erreurs;
	}

	@Override
	public Utilisateur saveUtilisateur(Utilisateur utilisateur) {
		utilisateur.setDateInscription(LocalDateTime.now());
		utilisateurDao.saveUtilisateur(utilisateur);
		return utilisateur;
	}

	@Override
	public List<Utilisateur> findAllUtilisateur() {
		return utilisateurDao.findAllUtilisateur();
	}

	@Override
	public boolean deleteByIdUtilisateur(int noUtilisateur) {
		return utilisateurDao.deleteByIdUtilisateur(noUtilisateur);
	}

	@Override
	public Utilisateur updateForgottenPassword(String email, String newMdp) {
		return utilisateurDao.updateForgottenPassword(email, newMdp);
	}

	@Override
	public boolean updateUtilisateur(Utilisateur utilisateur, String NewMdp) {
		return utilisateurDao.updateUtilisateur(utilisateur, NewMdp);
	}

	@Override
	public Utilisateur findByPseudo(String pseudo) {
		Utilisateur utilisateur = utilisateurDao.findByPseudo(pseudo);
		return utilisateur;
	}

	@Override
	public Utilisateur findByMailOrPseudoAndMdp(String idConnexion, String mdp) {
		return utilisateurDao.findByMailOrPseudoAndMdp(idConnexion, mdp);
	}

	@Override
	public boolean creditMaj(Utilisateur utilisateur, int montantEnchere) {
		boolean payement = false;
		
		if (utilisateur.getCredit() > montantEnchere) {
			int creditMaj = utilisateur.getCredit() - montantEnchere;
			utilisateur.setCredit(creditMaj);
			payement = utilisateurDao.creditMaj(utilisateur);
		}
		return payement ;
	}

	@Override
	public boolean recreditation(Utilisateur utilisateur, int montantEnchere) {
		if (utilisateur.getCredit() > montantEnchere) {
			int creditMaj = utilisateur.getCredit() + montantEnchere;
			utilisateur.setCredit(creditMaj);
		}
		return  utilisateurDao.creditMaj(utilisateur);
	}
}
