package fr.eni.ecole.Enibay.bll.bo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Utilisateur {

	private int noUtilisateur;
	private String pseudo;
	private String nom;
	private String prenom;
	private LocalDate ddn;
	private String email;
	private String telephone;
	private String rue;
	private String codePostal;
	private String ville;
	private String mdp;
	private int credit = 100;
	private boolean administrateur = false;
	private LocalDateTime dateInscription;

	public Utilisateur() {
		// TODO Auto-generated constructor stub
	}

//	public Utilisateur(int noUtilisateur, String pseudo, String nom, String prenom, LocalDate ddn, String email,
//			String telephone, String rue, String codePostal, String ville, String mdp, int credit,
//			boolean administrateur, LocalDateTime dateInscription) {
//		this.noUtilisateur = noUtilisateur;
//		this.pseudo = pseudo;
//		this.nom = nom;
//		this.prenom = prenom;
//		this.ddn = ddn;
//		this.email = email;
//		this.telephone = telephone;
//		this.rue = rue;
//		this.codePostal = codePostal;
//		this.ville = ville;
//		this.mdp = mdp;
//		this.credit = credit;
//		this.administrateur = administrateur;
//		this.dateInscription = dateInscription;
//	}
	
	public Utilisateur(String pseudo, String nom, String prenom, LocalDate ddn, String email,
			String telephone, String rue, String codePostal, String ville, String mdp) {
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.ddn = ddn;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.mdp = mdp;
	}
	
	public Utilisateur(String pseudo, String nom, String prenom, String email,
			String telephone, String rue, String codePostal, String ville, String mdp) {
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.mdp = mdp;
	}

	public int getNoUtilisateur() {
		return noUtilisateur;
	}

	public void setNoUtilisateur(int noUtilisateur) {
		this.noUtilisateur = noUtilisateur;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDdn() {
		return ddn;
	}

	public void setDdn(LocalDate ddn) {
		this.ddn = ddn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public boolean isAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(boolean administrateur) {
		this.administrateur = administrateur;
	}

	public LocalDateTime getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(LocalDateTime dateInscription) {
		this.dateInscription = dateInscription;
	}

	@Override
	public String toString() {
		return "Utilisateur [noUtilisateur=" + noUtilisateur + ", pseudo=" + pseudo + ", nom=" + nom + ", prenom="
				+ prenom + ", ddn=" + ddn + ", email=" + email + ", telephone=" + telephone + ", rue=" + rue
				+ ", codePostal=" + codePostal + ", ville=" + ville + ", mdp=" + mdp + ", credit=" + credit
				+ ", administrateur=" + administrateur + ", dateInscription=" + dateInscription + "]";
	}
	
	

	
	
	
}
