package fr.eni.ecole.Enibay.bll.managers;

import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface EnchereManager {

	//initialiser save
	Enchere saveEnchere(Enchere enchere);

	//Find Meilleure offre
	Enchere findEnchereByIdArticle(int noArticle);

	//Calcul le nouveau prix de vente
	int calculNewPrixVente(Enchere enchere);

	////Recherche du precedent enrechisseur
	 Enchere findAntepenultiemeEnchereByArticle(int numArticle);
	
	
	


}
