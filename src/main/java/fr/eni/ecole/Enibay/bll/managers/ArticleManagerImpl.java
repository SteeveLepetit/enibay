package fr.eni.ecole.Enibay.bll.managers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.EtatVente;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.dal.dao.ArticleDAO;
import fr.eni.ecole.Enibay.dal.dao.DAOFactory;

public class ArticleManagerImpl implements ArticleManager {

	private static ArticleManager instance = null;

	public static ArticleManager getInstance() {
		if (instance == null) {
			instance = new ArticleManagerImpl();
		}
		return instance;
	}

	private ArticleManagerImpl() {
	}

	private ArticleDAO articleDao = DAOFactory.getArticleDAO();

	@Override
	public Article saveArticle(Article article) {
		return articleDao.saveArticle(article);
	}

	@Override
	public List<Article> findAllArticle() {

		List<Article> articles = articleDao.findAllArticle();

		for (Article article : articles) {

			if ((LocalDateTime.now().isEqual(article.getDateDebutEnchere()))
					|| ((LocalDateTime.now().isAfter(article.getDateDebutEnchere()))
							&& ((LocalDateTime.now().isBefore(article.getDateFinEnchere()))))) {
				article.setEtatVente(EtatVente.EN_COURS);
				articleDao.updateEtatVente(article);
			}

			if ((LocalDateTime.now().isEqual(article.getDateFinEnchere()))
					|| (LocalDateTime.now().isAfter(article.getDateFinEnchere()))) {
				article.setEtatVente(EtatVente.TERMINE);
				articleDao.updateEtatVente(article);
			}
		}
		return articles;
	}

	public List<Article> findArticleByCategorie(String categorie) {
		return articleDao.findArticleByCategorie(categorie.toUpperCase());
	}

	@Override
	public Article findByNoArticle(int noArticle) {
		return articleDao.findByNoArticle(noArticle);
	}

	@Override
	public void updateEtatVente(Article article) {
		articleDao.updateEtatVente(article);
	}

/////////UPDATE LE PRIX DE VENTE DE L'ARTICLE
	@Override
	public void updatePrixVente(Article article, int newPrixVente) {
		articleDao.updatePrixVente(article, newPrixVente);

	}

///////////// BARRE DE RECHERCHE /////////////////////////////
	@Override
	public List<Article> findByNameArticle(String recherche) {
		return articleDao.findByNameArticle(recherche);
	}

	@Override
	public List<Article> findArticleOuvertes(Utilisateur utilisateurConnecte, List<String> lstEtatCheckbox) {

		List<Article> lstArticle = new ArrayList<>();

		if (lstEtatCheckbox.contains("ouvertes")) {
			List<Article> lstArticleEnchereOuverte = articleDao.findArticleEnchereOuverte(utilisateurConnecte);
			lstArticle.addAll(lstArticleEnchereOuverte);
		}

		if (lstEtatCheckbox.contains("mes_e")) {

			List<Article> lstArticleMesEncheres = articleDao.findArticleMesEncheres(utilisateurConnecte);

			
			Set<Article> set = lstArticleMesEncheres.stream()
					.collect(Collectors.toCollection(() ->new TreeSet<>
					(Comparator.comparing(Article::getNoArticle))));

			lstArticle.addAll(set);
		}

		if (lstEtatCheckbox.contains("mes_e_remp")) {
			List<Article> lstArticleMesEncheres = articleDao.findArticleMesEncheresRemportees(utilisateurConnecte);
			lstArticle.addAll(lstArticleMesEncheres);
		}

		if (lstEtatCheckbox.contains("encours")) {
			List<Article> lstArticleEnchereOuverte = articleDao.findArticleByVendeurAndEtat(utilisateurConnecte,
					EtatVente.EN_COURS);
			lstArticle.addAll(lstArticleEnchereOuverte);
		}

		if (lstEtatCheckbox.contains("nondebutees")) {
			List<Article> lstArticleMesEncheres = articleDao.findArticleByVendeurAndEtat(utilisateurConnecte,
					EtatVente.NON_COMMENCE);
			lstArticle.addAll(lstArticleMesEncheres);
		}

		if (lstEtatCheckbox.contains("terminees")) {
			List<Article> lstArticleMesEncheres = articleDao.findArticleByVendeurAndEtat(utilisateurConnecte,
					EtatVente.TERMINE);
			lstArticle.addAll(lstArticleMesEncheres);
		}

		List<Article> lstArticlesSansDoublons = lstArticle.stream().distinct().collect(Collectors.toList());
		return lstArticlesSansDoublons;
	}

	@Override
	public Map<String, String> verifArticle(Article a) {
		Map<String, String> erreurs = new HashMap<>();

		erreurs.clear();

		if (a.getNomArticle().equals("")) {
			erreurs.put("nom", "Veuillez entrer un titre pour l'article");
		}
		if (a.getDateDebutEnchere().isAfter(a.getDateFinEnchere())) {
			erreurs.put("date", "Veuillez entrer une date de d�but ant�rieure � la date de fin");
		}
		if (a.getRueRetrait().isEmpty() || a.getRueRetrait().isEmpty() || a.getCodePostalRetrait().isEmpty()) {
			erreurs.put("retrait", "Veuillez compl�ter l'adresse de retrait");
		}
		if (String.valueOf(a.getMiseAPrix()).equalsIgnoreCase("")) {
			erreurs.put("miseAPrix", "Veuillez choisir une mise � prix de d�part");
		}
		if (a.getCategorie().toString().equals("")) {
			erreurs.put("categorie", "Veuillez choisir une cat�gorie");
		}

		return erreurs;
	}

}
