package fr.eni.ecole.Enibay.bll.bo;

import java.time.LocalDateTime;

public class Enchere {

	private int noEnchere;
	private LocalDateTime date;
	private int montantEnchere;
	private Article article;
	private Utilisateur utilisateur;
	
	
	public Enchere() {
		// TODO Auto-generated constructor stub
	}


	public Enchere(int montant, Article article, Utilisateur utilisateur) {
		super();
		this.montantEnchere = montant;
		this.article = article;
		this.utilisateur = utilisateur;
	}


	public int getNoEnchere() {
		return noEnchere;
	}


	public void setNoEnchere(int noEnchere) {
		this.noEnchere = noEnchere;
	}


	public LocalDateTime getDate() {
		return date;
	}


	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public int getMontantEnchere() {
		return montantEnchere;
	}


	public void setMontantEnchere(int montantEnchere) {
		this.montantEnchere = montantEnchere;
	}


	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}


	public Utilisateur getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}


	@Override
	public String toString() {
		return "Enchere [noEnchere=" + noEnchere + ", date=" + date + ", montantEnchere=" + montantEnchere
				+ ", article=" + article + ", utilisateur=" + utilisateur + "]";
	}


	
	
	
}
