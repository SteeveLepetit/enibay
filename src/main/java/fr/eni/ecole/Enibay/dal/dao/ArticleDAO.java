package fr.eni.ecole.Enibay.dal.dao;

import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.EtatVente;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface ArticleDAO {
		//////////////////////INSERT ////////////////
		//INITIALISER - SAVE
		Article saveArticle(Article article);
		
		//////////////////////SELECT ////////////////
		//LIST TOUS LES ARTICLES
		List <Article> findAllArticle();
		
		////// FIND BY CATEGORIE
		List<Article> findArticleByCategorie(String categorie);
		
		//FIND BY ID
		Article findByNoArticle(int noArticle);
		
		//////////////////////UPDATE ////////////////
		//UPDATE 
		void updateEtatVente(Article article);

		void updatePrixVente(Article article,  int newPrixVente);

		/////////////BARRE DE RECHERCHE///////////////////////
		List<Article> findByNameArticle(String recherche);

		//Liste article encheree ouverte
		List<Article> findArticleEnchereOuverte(Utilisateur utilisateurConnecte);

		//Liste article mes encheres
		List<Article> findArticleMesEncheres(Utilisateur utilisateurConnecte);
		// Liste mes encheres remportees
		List<Article> findArticleMesEncheresRemportees(Utilisateur utilisateurConnecte);

		
		/////////////CHECKBOX MES VENTES///////////////////////
		List<Article> findArticleByVendeurAndEtat(Utilisateur utilisateurConnecte, EtatVente enCours);
}
