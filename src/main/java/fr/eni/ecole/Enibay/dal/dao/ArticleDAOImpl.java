package fr.eni.ecole.Enibay.dal.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Categorie;
import fr.eni.ecole.Enibay.bll.bo.EtatVente;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public class ArticleDAOImpl implements ArticleDAO{

////////////SINGLETON ////////////////////////
	private static ArticleDAO instance = null;

	public static ArticleDAO getInstance() {
		if (instance == null) {
			instance = new ArticleDAOImpl();
		}
		return instance;
	}

	private ArticleDAOImpl() {
	}
	
	@Override
	public Article saveArticle(Article article) {
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = "INSERT INTO Articles VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, article.getNomArticle());
			pstmt.setString(2, article.getDescription());
			pstmt.setTimestamp(3, Timestamp.valueOf(article.getDateDebutEnchere()));
			pstmt.setTimestamp(4, Timestamp.valueOf(article.getDateFinEnchere()));
			pstmt.setInt(5, article.getMiseAPrix());
			pstmt.setInt(6, article.getPrixVente());
			pstmt.setInt(7, article.getUtilisateur().getNoUtilisateur());
			pstmt.setInt(8, article.getCategorie().getNoCategorie());
			pstmt.setString(9, article.getRueRetrait());
			pstmt.setString(10, article.getCodePostalRetrait());
			pstmt.setString(11, article.getVilleRetrait());
			pstmt.setString(12, article.getEtatVente().name());
			
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
				article.setNoArticle(rs.getInt(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return article;
	}
/////////////////////// AFFICHAGE /////////////////////
	@Override
	public List<Article> findAllArticle() {
		List<Article> articles = new ArrayList<>();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
			"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE  A.etatVente ='EN_COURS'";
			PreparedStatement pstmt = con.prepareStatement(QUERY);

			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));

				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				//FAIRE LE JOIN ?
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// AFFICHAGE PAR CATEGORIE /////////////////////	
	public List<Article> findArticleByCategorie(String categorie) {
		List<Article> articles = new ArrayList<>();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
			"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie "
			+ "WHERE libelle = ?";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, categorie);
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				//SET OBJET // JOIN ?
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				//FAIRE LE JOIN ?
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public Article findByNoArticle(int noArticle) {
		Article article = new Article();
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
			"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie "
			+ "WHERE noArticle = ?";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setInt(1, noArticle);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				//SET OBJET // JOIN ?
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				//FAIRE LE JOIN ?
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return article;
	}

	@Override
	public void updateEtatVente(Article article) {
				
				try (Connection con = PoolConnexion.getConnexion("Enibay")) {

					final String QUERY = "UPDATE Articles SET EtatVente = ? WHERE noArticle = ?";
					
					PreparedStatement pstmt = con.prepareStatement(QUERY);
					pstmt.setString(1, article.getEtatVente().name());
					pstmt.setInt(2, article.getNoArticle());
					
					pstmt.executeUpdate();
					

				} catch (Exception e) {
					e.printStackTrace();
				}

	}
////////////////////////////////////////////////////////////////////////
///////////////UPDATE LE PRIX DE L'ARTICLE A CHAQUE ENCHERE
	@Override
	public void updatePrixVente(Article article, int newPrixVente) {
			
			try (Connection con = PoolConnexion.getConnexion("Enibay")) {

				final String QUERY = "UPDATE Articles SET prixVente = ? WHERE noArticle = ?";
				
				PreparedStatement pstmt = con.prepareStatement(QUERY);
				pstmt.setInt(1, newPrixVente);
				pstmt.setInt(2, article.getNoArticle());
				
				pstmt.executeUpdate();
				

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	
	//////////////// BARRE DE RECHERCHE //////////////////////////////
	@Override
	public List<Article> findByNameArticle(String recherche) {
		List<Article> articles = new ArrayList<>();

		//recherche = "'%" + recherche + "%'";
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
			"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE nomArticle LIKE ?"; //'%pc%'
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, "%"+recherche+"%");
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	}

//// Liste achats enchere ouverte
	@Override
	public List<Article> findArticleEnchereOuverte(Utilisateur utilisateurConnecte) {
		List<Article> articles = new ArrayList<>();

		//recherche = "'%" + recherche + "%'";
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
					"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE etatVente = 'EN_COURS'";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	
	}

	// Liste achats mes encheres
	@Override
	public List<Article> findArticleMesEncheres(Utilisateur utilisateurConnecte) {
		List<Article> articles = new ArrayList<>();

		//recherche = "'%" + recherche + "%'";
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 
					"SELECT * FROM ARTICLES A JOIN ENCHERES E ON A.noArticle = E.noArticle JOIN UTILISATEURS U ON U.noUtilisateur=E.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE A.etatVente = 'EN_COURS' AND E.noUtilisateur = ? ORDER BY A.noArticle DESC";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setInt(1, utilisateurConnecte.getNoUtilisateur());
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	}

	@Override
	public List<Article> findArticleMesEncheresRemportees(Utilisateur utilisateurConnecte) {

		
		List<Article> articles = new ArrayList<>();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = "SELECT * FROM ARTICLES A JOIN ENCHERES E ON A.noArticle = E.noArticle JOIN UTILISATEURS U ON U.noUtilisateur=E.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE A.etatVente = 'TERMINE' and E.noUtilisateur = ? and E.montantEnchere=A.prixVente";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setInt(1, utilisateurConnecte.getNoUtilisateur());
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
	}


	
	/////Liste mes ventes en cours A FAIRE
	@Override
	public List<Article> findArticleByVendeurAndEtat(Utilisateur utilisateurConnecte, EtatVente etat) {
		List<Article> articles = new ArrayList<>();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = 			
			"SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur JOIN CATEGORIES C ON A.noCategorie = C.noCategorie WHERE A.etatVente = ? and A.noUtilisateur = ? ";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, etat.name());
			pstmt.setInt(2, utilisateurConnecte.getNoUtilisateur());
			
			ResultSet rs = pstmt.executeQuery();

			
			while (rs.next()) {
				Article article = new Article();
				article.setNoArticle(rs.getInt("noArticle"));
				article.setNomArticle(rs.getString("nomArticle"));
				article.setDescription(rs.getString("description"));
				article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
				article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
				article.setMiseAPrix(rs.getInt("miseAPrix"));
				article.setPrixVente(rs.getInt("prixVente"));
				
				Utilisateur utilisateur = new Utilisateur();
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				article.setUtilisateur(utilisateur);
				
				article.setCategorie(Categorie.valueOf(rs.getString("libelle")));
				
				article.setRueRetrait(rs.getString("rueRetrait"));
				article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
				article.setVilleRetrait(rs.getString("villeRetrait"));
				article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				articles.add(article);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return articles;
		
	}

	
		
	
}
