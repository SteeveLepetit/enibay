package fr.eni.ecole.Enibay.dal.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public class UtilisateurDAOImpl implements UtilisateurDAO {

	//////////// SINGLETON ////////////////////////
	private static UtilisateurDAO instance = null;
	
	public static UtilisateurDAO getInstance() {
		if (instance == null) {
			instance = new UtilisateurDAOImpl();
		}
		return instance;
	}

	private UtilisateurDAOImpl() {
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////SAVE////////////////////////////	
	@Override
	public Utilisateur saveUtilisateur(Utilisateur utilisateur) {
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = "INSERT INTO Utilisateurs VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setDate(4, Date.valueOf(utilisateur.getDdn()));
			pstmt.setString(5, utilisateur.getEmail());
			pstmt.setString(6, utilisateur.getTelephone());
			pstmt.setString(7, utilisateur.getRue());
			pstmt.setString(8, utilisateur.getCodePostal());
			pstmt.setString(9, utilisateur.getVille());
			pstmt.setString(10, utilisateur.getMdp());
			pstmt.setInt(11, utilisateur.getCredit());
			pstmt.setBoolean(12, utilisateur.isAdministrateur());
			pstmt.setTimestamp(13, Timestamp.valueOf(utilisateur.getDateInscription()));

			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
				utilisateur.setNoUtilisateur(rs.getInt(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<Utilisateur> findAllUtilisateur() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Utilisateur findByPseudo(String pseudo) {
		Utilisateur utilisateur = new Utilisateur();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = "SELECT * FROM Utilisateurs WHERE pseudo = ?";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, pseudo);

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////CONNEXION////////////////////////////		
	@Override
	public Utilisateur findByMailOrPseudoAndMdp(String idConnexion, String mdp) {
		Utilisateur utilisateur = new Utilisateur();

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {
			final String QUERY = "SELECT * FROM Utilisateurs WHERE (email = ? AND mdp = ?) OR (pseudo = ? AND mdp = ?)";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, idConnexion);
			pstmt.setString(2, mdp);
			pstmt.setString(3, idConnexion);
			pstmt.setString(4, mdp);

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
				utilisateur.setPseudo(rs.getString("pseudo"));
				utilisateur.setNom(rs.getString("nom"));
				utilisateur.setPrenom(rs.getString("prenom"));
				utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
				utilisateur.setEmail(rs.getString("email"));
				utilisateur.setTelephone(rs.getString("telephone"));
				utilisateur.setRue(rs.getString("rue"));
				utilisateur.setCodePostal(rs.getString("codePostal"));
				utilisateur.setVille(rs.getString("ville"));
				utilisateur.setMdp(rs.getString("mdp"));
				utilisateur.setCredit(rs.getInt("credit"));
				utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
				utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean deleteByIdUtilisateur(int noUtilisateur) {
		boolean isDelete = false;

		try (Connection con = PoolConnexion.getConnexion("Enibay")) {

			final String QUERY = "DELETE FROM Utilisateurs WHERE noUtilisateur = ?";
			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setInt(1, noUtilisateur);

			isDelete = pstmt.executeUpdate() > 0;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isDelete;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////// UPDATE ////////////////////////////		
	@Override
	public boolean updateUtilisateur(Utilisateur utilisateur, String NewMdp) {
		boolean isUpdated = false;
		
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {

			// 2 - Fabriquer la requete SQL
			final String QUERY = "UPDATE Utilisateurs SET pseudo = ?, nom = ?, prenom = ?, email = ?"
					+ ", telephone = ?, rue = ?, codePostal = ?, ville = ?, mdp = ? WHERE noUtilisateur = ?";
			// role = ?

			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6, utilisateur.getRue());
			pstmt.setString(7, utilisateur.getCodePostal());
			pstmt.setString(8, utilisateur.getVille());
			pstmt.setString(9, NewMdp);
			pstmt.setInt(10, utilisateur.getNoUtilisateur());

			pstmt.executeUpdate();
			
			isUpdated = true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isUpdated;
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public Utilisateur updateForgottenPassword(String email, String newMdp) {
		// TODO Auto-generated method stub
		return null;
	}


/////////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean creditMaj(Utilisateur utilisateur) {
		boolean isUpdated = false;
		try (Connection con = PoolConnexion.getConnexion("Enibay")) {

			// 2 - Fabriquer la requete SQL
			final String QUERY = "UPDATE Utilisateurs SET credit = ? WHERE noUtilisateur = ?";
			

			PreparedStatement pstmt = con.prepareStatement(QUERY);
			pstmt.setInt(1, utilisateur.getCredit());
			pstmt.setInt(2, utilisateur.getNoUtilisateur());

			isUpdated = pstmt.executeUpdate() > 0;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return isUpdated;
	}

}
