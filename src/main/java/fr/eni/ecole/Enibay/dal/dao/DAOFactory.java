package fr.eni.ecole.Enibay.dal.dao;

public abstract class DAOFactory {
	
		public static UtilisateurDAO getUtilisateurDAO() {
			return UtilisateurDAOImpl.getInstance();
		}
		
		
		public static ArticleDAO getArticleDAO() {
			return ArticleDAOImpl.getInstance();
		}
		
		
		public static EnchereDAO getEnchereDAO() {
			return EnchereDAOImpl.getInstance();
		}
	}


