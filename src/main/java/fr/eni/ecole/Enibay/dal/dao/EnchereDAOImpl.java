package fr.eni.ecole.Enibay.dal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Categorie;
import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.EtatVente;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public class EnchereDAOImpl implements EnchereDAO {

		//////////// SINGLETON ////////////////////////
		private static EnchereDAO instance = null;

		public static EnchereDAO getInstance() {
			if (instance == null) {
				instance = new EnchereDAOImpl();
			}
			return instance;
		}

		private EnchereDAOImpl() {
		}

		@Override
		public Enchere saveEnchere(Enchere enchere) {
			
			try (Connection con = PoolConnexion.getConnexion("Enibay")) {
				final String QUERY = "INSERT INTO Encheres VALUES (?,?,?,?)";
				PreparedStatement pstmt = con.prepareStatement(QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
				pstmt.setTimestamp(1, Timestamp.valueOf(enchere.getDate()));
				pstmt.setInt(2, enchere.getMontantEnchere());
				pstmt.setInt(3, enchere.getArticle().getNoArticle());
				pstmt.setInt(4, enchere.getUtilisateur().getNoUtilisateur());
						
				pstmt.executeUpdate();

				ResultSet rs = pstmt.getGeneratedKeys();
				while (rs.next()) {
					enchere.setNoEnchere(rs.getInt(1));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return enchere;
		}
		
		public Enchere findEnchereByIdArticle(int noArticle) {

			Enchere enchere = new Enchere();
			try (Connection con = PoolConnexion.getConnexion("Enibay")) {
				final String QUERY = 
						"SELECT * FROM ENCHERES E "
						+ "INNER JOIN UTILISATEURS U	"
						+ "ON E.noUtilisateur = U.noUtilisateur "
						+ "JOIN ARTICLES A "
						+ "ON A.noArticle = E.noArticle "
						+ "WHERE E.noArticle = ?	"
						+ "ORDER BY montantEnchere DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";
				PreparedStatement pstmt = con.prepareStatement(QUERY);
				pstmt.setInt(1, noArticle);
				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					enchere.setNoEnchere(rs.getInt("noEnchere"));
					enchere.setDate((rs.getTimestamp("dateEnchere")).toLocalDateTime());
					enchere.setMontantEnchere(rs.getInt("montantEnchere"));
										
					Article article = new Article();
					article.setNoArticle(rs.getInt("noArticle"));
					article.setNomArticle(rs.getString("nomArticle"));
					article.setDescription(rs.getString("description"));
					article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
					article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
					article.setMiseAPrix(rs.getInt("miseAPrix"));
					article.setPrixVente(rs.getInt("prixVente"));
					article.setRueRetrait(rs.getString("rueRetrait"));
					article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
					article.setVilleRetrait(rs.getString("villeRetrait"));
					article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
					enchere.setArticle(article);
					
					Utilisateur utilisateur = new Utilisateur();
					utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
					utilisateur.setPseudo(rs.getString("pseudo"));
					utilisateur.setNom(rs.getString("nom"));
					utilisateur.setPrenom(rs.getString("prenom"));
					utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
					utilisateur.setEmail(rs.getString("email"));
					utilisateur.setTelephone(rs.getString("telephone"));
					utilisateur.setRue(rs.getString("rue"));
					utilisateur.setCodePostal(rs.getString("codePostal"));
					utilisateur.setVille(rs.getString("ville"));
					utilisateur.setMdp(rs.getString("mdp"));
					utilisateur.setCredit(rs.getInt("credit"));
					utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
					utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
					//
					enchere.setUtilisateur(utilisateur);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return enchere;
		}
		
		@Override
		public Enchere findAntepenultiemeEnchereByArticle(int numArticle) {
			Enchere enchere = new Enchere();	
			
			try (Connection con = PoolConnexion.getConnexion("Enibay")) {
				final String QUERY = 
						"SELECT * FROM ENCHERES E "  
						+ "INNER JOIN UTILISATEURS U "	
						+ "ON E.noUtilisateur = U.noUtilisateur "
						+ "JOIN ARTICLES A "
						+ "ON A.noArticle = E.noArticle "
						+ "WHERE E.noArticle = ? "
						+ "ORDER BY dateEnchere DESC "
						+ "OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY";
												
				PreparedStatement pstmt = con.prepareStatement(QUERY);
				pstmt.setInt(1, numArticle);
				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					enchere.setNoEnchere(rs.getInt("noEnchere"));
					enchere.setDate((rs.getTimestamp("dateEnchere")).toLocalDateTime());
					enchere.setMontantEnchere(rs.getInt("montantEnchere"));

					Article article = new Article();
					article.setNoArticle(rs.getInt("noArticle"));
					article.setNomArticle(rs.getString("nomArticle"));
					article.setDescription(rs.getString("description"));
					article.setDateDebutEnchere((rs.getTimestamp("dateDebutEncheres")).toLocalDateTime());
					article.setDateFinEnchere((rs.getTimestamp("dateFinEncheres")).toLocalDateTime());
					article.setMiseAPrix(rs.getInt("miseAPrix"));
					article.setPrixVente(rs.getInt("prixVente"));
					article.setRueRetrait(rs.getString("rueRetrait"));
					article.setCodePostalRetrait(rs.getString("codePostalRetrait"));
					article.setVilleRetrait(rs.getString("villeRetrait"));
					article.setEtatVente(EtatVente.valueOf(rs.getString("etatVente")));
				
					enchere.setArticle(article);
					
					Utilisateur utilisateur = new Utilisateur();
					utilisateur.setNoUtilisateur(rs.getInt("noUtilisateur"));
					utilisateur.setPseudo(rs.getString("pseudo"));
					utilisateur.setNom(rs.getString("nom"));
					utilisateur.setPrenom(rs.getString("prenom"));
					utilisateur.setDdn(rs.getDate("ddn").toLocalDate());
					utilisateur.setEmail(rs.getString("email"));
					utilisateur.setTelephone(rs.getString("telephone"));
					utilisateur.setRue(rs.getString("rue"));
					utilisateur.setCodePostal(rs.getString("codePostal"));
					utilisateur.setVille(rs.getString("ville"));
					utilisateur.setMdp(rs.getString("mdp"));
					utilisateur.setCredit(rs.getInt("credit"));
					utilisateur.setAdministrateur(rs.getBoolean("administrateur"));
					utilisateur.setDateInscription((rs.getTimestamp("dateInscription")).toLocalDateTime());
					//
					enchere.setUtilisateur(utilisateur);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return enchere;
		}

	}

