package fr.eni.ecole.Enibay.dal.dao;

import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface EnchereDAO {
	//////////////////////INSERT ////////////////
	//INITIALISER - SAVE
	Enchere saveEnchere(Enchere enchere);
	
	//FIND MEILLEURE OFFRE
	Enchere findEnchereByIdArticle(int noArticle); 

	//Recherche du precedent enrechisseur
	 Enchere findAntepenultiemeEnchereByArticle(int numArticle);



	
}
