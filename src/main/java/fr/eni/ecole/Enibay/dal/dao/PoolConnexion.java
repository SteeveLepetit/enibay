package fr.eni.ecole.Enibay.dal.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PoolConnexion {
	
	//POUR L'INSTANT LOCALHOST (voir avec Yann pour une seule BDD)
	public static Connection getConnexion(String database) throws ClassNotFoundException, SQLException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); // charger le driver
		String connectionUrl = "jdbc:sqlserver://10.43.101.1;database=" + database + ";";
		Connection con = DriverManager.getConnection(connectionUrl, "sa", "Pa$$w0rd");
		return con;
	}

}
