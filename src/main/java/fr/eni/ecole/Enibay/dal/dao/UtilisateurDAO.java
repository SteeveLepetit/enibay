package fr.eni.ecole.Enibay.dal.dao;

import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;

public interface UtilisateurDAO {
	//////////////////////INSERT ////////////////
	//INSCRIPTION - SAVE
	Utilisateur saveUtilisateur(Utilisateur utilisateur);
	
	//////////////////////SELECT ////////////////
	//LIST TOUS LES UTILISATEURS
	List <Utilisateur> findAllUtilisateur();
		
	//FIND BY PSEUDO
	Utilisateur findByPseudo(String pseudo);
	
	//CONNEXION
	Utilisateur findByMailOrPseudoAndMdp(String idConnexion, String mdp);
	
	//////////////////////DELETE ////////////////	
	//SUPPRIME PAR ID
	boolean deleteByIdUtilisateur(int noUtilisateur);
	
	//////////////////////UPDATE ////////////////
	//UPDATE 
	boolean updateUtilisateur(Utilisateur utilisateur, String NewMdp);
	
	//MDP OUBLIE
	Utilisateur updateForgottenPassword(String email, String newMdp);

	//Modification credit 
	boolean creditMaj(Utilisateur utilisateur);

	

}
