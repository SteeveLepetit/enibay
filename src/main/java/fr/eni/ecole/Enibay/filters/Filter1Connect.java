package fr.eni.ecole.Enibay.filters;

import java.io.IOException;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = "/connect/*")
public class Filter1Connect implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		// la m�thode qui sera appel� automatiquement
		// au d�clenchement du filtre
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpServletRequest request = (HttpServletRequest) req;
		
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");

		if (utilisateur != null) { // si je suis connect� ici simplifi�
			filterChain.doFilter(req, resp); // laisse passer la requete			
		} else {
			response.sendRedirect(
					request.getContextPath() + "/index");
		}
	}

}
