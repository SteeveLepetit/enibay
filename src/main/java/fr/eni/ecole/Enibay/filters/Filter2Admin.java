package fr.eni.ecole.Enibay.filters;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = "/connect/admin/*")
public class Filter2Admin implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		// la m�thode qui sera appel� automatiquement
		// au d�clenchement du filtre
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpServletRequest request = (HttpServletRequest) req;
		
		if(false) {
			filterChain.doFilter(req, resp);
		} else {
			response.sendRedirect(
					request.getContextPath() + "/accueil");
		}
		
	}

}
