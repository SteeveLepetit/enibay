package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/inscription")
public class InscriptionControler extends HttpServlet {


	UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext()
	    .getRequestDispatcher("/jsp/inscription.jsp")
	    .forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String pseudo = req.getParameter("pseudo");
		String nom = req.getParameter("nom");
		String prenom = req.getParameter("prenom");
		String ddn = req.getParameter("ddn");
		String email = req.getParameter("email");
		String telephone = req.getParameter("telephone");
		String rue = req.getParameter("rue");
		String codePostal = req.getParameter("codePostal");
		String ville = req.getParameter("ville");
		String mdp = req.getParameter("mdp");
		String mdpconf = req.getParameter("mdpConf");
		
		Utilisateur utilisateur = new Utilisateur(pseudo, nom, prenom, LocalDate.parse(ddn), email, telephone, rue ,codePostal, ville, mdp);
		
		Map<String, String> erreurs = utilisateurManager.verifInscription(utilisateur, mdpconf);
		
		if (erreurs.isEmpty()) {
			//s'il n'y a pas d'erreur : on l'enregistre
			utilisateurManager.saveUtilisateur(utilisateur);
			this.getServletContext().getRequestDispatcher("/jsp/connexion.jsp").forward(req, resp);
		} else {
			req.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/jsp/inscription.jsp").forward(req, resp);
		}

	}
	
	

}
