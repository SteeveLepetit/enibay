package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Enchere;
import fr.eni.ecole.Enibay.bll.bo.EtatVente;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ArticleManager;
import fr.eni.ecole.Enibay.bll.managers.EnchereManager;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/connect/details-vente")
public class DetailVenteControler extends HttpServlet {

	private EnchereManager enchereManager = ManagerFactory.getEnchereManager();
	private ArticleManager articleManager = ManagerFactory.getArticleManager();
	private UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int numArticle = Integer.parseInt(req.getParameter("noArticle"));
		Article article = articleManager.findByNoArticle(numArticle);
		Utilisateur utilisateurConnecte = (Utilisateur) req.getSession().getAttribute("utilisateur");
		req.setAttribute("article", article);
		Enchere enchere = enchereManager.findEnchereByIdArticle(numArticle);
		Enchere meilleureOffre = enchereManager.findEnchereByIdArticle(numArticle);
		boolean enCours = false;
		boolean termine = false;
		boolean nomGagnant = false;
		boolean nonCommence = false;
		if (article.getEtatVente() == EtatVente.NON_COMMENCE) {
			nonCommence = true;
		}
		if (article.getEtatVente() == EtatVente.EN_COURS) {
			enCours = true;
		}

		if (article.getEtatVente() == EtatVente.TERMINE) {
			termine = true;
			if (meilleureOffre.getUtilisateur().getPseudo().equals(utilisateurConnecte.getPseudo())) {
				nomGagnant=true;
			}
		}

		req.setAttribute("nomGagnant", nomGagnant);
		req.setAttribute("nonCommence", nonCommence);
		req.setAttribute("enCours", enCours);
		req.setAttribute("termine", termine);
		req.setAttribute("meilleureOffre", meilleureOffre);

		this.getServletContext().getRequestDispatcher("/jsp/details-vente.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int montantEnchere = Integer.parseInt(req.getParameter("encherir"));
		int numArticle = Integer.parseInt(req.getParameter("noArticle"));
		Article article = articleManager.findByNoArticle(numArticle);

		req.setAttribute("article", article);
		Utilisateur utilisateurConnecte = (Utilisateur) req.getSession().getAttribute("utilisateur");
		Enchere enchere = new Enchere(montantEnchere, article, utilisateurConnecte);
		boolean enCours = false;
		boolean termine = false;
		boolean nonCommence = false;
		if (article.getEtatVente() == EtatVente.NON_COMMENCE) {
			nonCommence = true;
		}
		if (article.getEtatVente() == EtatVente.EN_COURS) {
			enCours = true;
		}

		if (article.getEtatVente() == EtatVente.TERMINE) {
			termine = true;
		}
		req.setAttribute("nonCommence", nonCommence);
		req.setAttribute("enCours", enCours);
		req.setAttribute("termine", termine);
		req.setAttribute("enchere", enchere);


		if (utilisateurManager.creditMaj(utilisateurConnecte, montantEnchere)) {
//SI le cr�dit utilisateur a �t� updat� :
//on recr�dite l'avant dernier acqu�reur
			
			if (montantEnchere >= article.getPrixVente()) {
			enchereManager.saveEnchere(enchere);
			int newPrixVente = enchereManager.calculNewPrixVente(enchere);
				articleManager.updatePrixVente(enchere.getArticle(), newPrixVente);
			}else {
				req.setAttribute("erreur", "Vous devez saisir un nombre sup�rieur � la meilleure offre ");
			}

			article = articleManager.findByNoArticle(numArticle);

			req.setAttribute("article", article);
			Enchere avDerniereEnchere = enchereManager.findAntepenultiemeEnchereByArticle(numArticle);
			int creditAvDerniereEnchere = avDerniereEnchere.getMontantEnchere();
			Utilisateur utilisateurAvDerniereEnchere = avDerniereEnchere.getUtilisateur();
			if (utilisateurAvDerniereEnchere != null) {
				utilisateurManager.recreditation(utilisateurAvDerniereEnchere, creditAvDerniereEnchere);
			}
			
			Enchere meilleureOffre = enchereManager.findEnchereByIdArticle(numArticle);
			req.setAttribute("meilleureOffre", meilleureOffre);
		} else {
			req.setAttribute("erreur", "Vous n'avez pas assez de cr�dit ! ");
		}
		this.getServletContext().getRequestDispatcher("/jsp/details-vente.jsp").forward(req, resp);
	}

}
