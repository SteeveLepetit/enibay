package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;
import java.time.LocalDate;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/connect/modifier-profil")
public class ModifierMonProfilControler extends HttpServlet {

	private UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				
		
		this.getServletContext().getRequestDispatcher("/jsp/modifier-profil.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Utilisateur utilisateurConnecte = (Utilisateur) req.getSession().getAttribute("utilisateur");
		String message = "Erreur lors du changement de mot de passe";
		String pseudo;
		String nom;
		String prenom;
		String email;
		String codePostal;
		String rue;
		String ville;
		String telephone;
		String NMdp = utilisateurConnecte.getMdp();
		
		if (req.getParameter("action") != null) //if action is not null
        {
            String action = req.getParameter("action");
            if (action.equals("modifier")) //add button clicked
            {
        		if (!(req.getParameter("pseudo").equals(""))) {
        			pseudo = req.getParameter("pseudo");
        		} else {
        			pseudo = utilisateurConnecte.getPseudo();
        		}
        		if (!(req.getParameter("nom").equals(""))) {
        			nom = req.getParameter("nom");
        		} else {
        			nom = utilisateurConnecte.getNom();
        		}
        		if (!(req.getParameter("prenom").equals(""))) {
        			prenom = req.getParameter("prenom");
        		} else {
        			prenom = utilisateurConnecte.getPrenom();
        		}
        		if (!(req.getParameter("email").equals(""))) {
        			email = req.getParameter("email");
        		} else {
        			email = utilisateurConnecte.getEmail();
        		}
        		if (!(req.getParameter("codePostal").equals(""))) {
        			codePostal = req.getParameter("codePostal");
        		} else {
        			codePostal = utilisateurConnecte.getCodePostal();
        		}
        		if (!(req.getParameter("rue").equals(""))) {
        			rue = req.getParameter("rue");
        		} else {
        			rue = utilisateurConnecte.getRue();
        		}
        		if (!(req.getParameter("ville").equals(""))) {
        			ville = req.getParameter("ville");
        		} else {
        			ville = utilisateurConnecte.getVille();
        		}
        		if (!(req.getParameter("telephone").equals(""))) {
        			telephone = req.getParameter("telephone");
        		} else {
        			telephone = utilisateurConnecte.getTelephone();
        		}


        		if ((!(req.getParameter("mdp") == null) && (req.getParameter("mdp").equals(utilisateurConnecte.getMdp())))) {
        			
        			
        			if (!(req.getParameter("newmdp").equals(""))
        					&& ((req.getParameter("confirmation").equals(req.getParameter("newmdp"))))) {
        				NMdp = req.getParameter("newmdp");
        			} else {
        				req.setAttribute("message", message);
        				this.getServletContext().getRequestDispatcher("/jsp/modifier-profil.jsp").forward(req, resp);
        			}
        		}

        		Utilisateur utilisateurModifier = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, codePostal, ville,
        				NMdp);
        		utilisateurModifier.setCredit(utilisateurConnecte.getCredit());

        		utilisateurModifier.setNoUtilisateur(utilisateurConnecte.getNoUtilisateur());
        		boolean isUpdated = utilisateurManager.updateUtilisateur(utilisateurModifier, NMdp);
        		
        		if(isUpdated) {
        			req.getSession().setAttribute("utilisateur", utilisateurModifier);
        			
        		}
        		this.getServletContext()
        		.getRequestDispatcher("/jsp/mon-profil.jsp")
        		.forward(req, resp);
            }
            else if (action.equals("supprimer"))
            {
            	utilisateurConnecte.setNoUtilisateur(utilisateurConnecte.getNoUtilisateur());
            	boolean isDeleted = utilisateurManager.deleteByIdUtilisateur(utilisateurConnecte.getNoUtilisateur());
            	
            	if(isDeleted) {
            		this.getServletContext()
            		.getRequestDispatcher("/jsp/index.jsp")
            		.forward(req, resp);
            	}
            }
        }

	}

}
