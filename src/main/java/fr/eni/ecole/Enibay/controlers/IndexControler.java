package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ArticleManager;
import fr.eni.ecole.Enibay.bll.managers.EnchereManager;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/index")
public class IndexControler extends HttpServlet {

	private UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();
	private EnchereManager enchereManager = ManagerFactory.getEnchereManager();
	private ArticleManager articleManager = ManagerFactory.getArticleManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<Article> lstArticleRecherche = articleManager.findAllArticle();
		req.setAttribute("lstArticleRecherche", lstArticleRecherche);
		this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String recherche = req.getParameter("search");
		List<Article> lstArticleRecherche = new ArrayList<>();
		List<String> lstEtatCheckbox = new ArrayList<>();

		String categorie = req.getParameter("categorie"); // "zero"

		String ouvertes = req.getParameter("ouvertes");
		String mes_e = req.getParameter("mes_e");
		String mes_e_remp = req.getParameter("mes_e_remp");

		String encours = req.getParameter("encours");
		String nondebutees = req.getParameter("nondebutees");
		String terminees = req.getParameter("terminees");

		Utilisateur utilisateurConnecte = (Utilisateur) req.getSession().getAttribute("utilisateur");

		if (ouvertes != null) {
			lstEtatCheckbox.add(ouvertes);
		}

		if (mes_e != null) {
			lstEtatCheckbox.add(mes_e);
		}

		if (mes_e_remp != null) {
			lstEtatCheckbox.add(mes_e_remp);
		}

		if (encours != null) {
			lstEtatCheckbox.add(encours);
		}

		if (nondebutees != null) {
			lstEtatCheckbox.add(nondebutees);
		}

		if (terminees != null) {
			lstEtatCheckbox.add(terminees);
		}

		List<Article> lstArticleOuvertes = articleManager.findArticleOuvertes(utilisateurConnecte, lstEtatCheckbox);

		if (!recherche.equals("") || categorie != null || !categorie.equals("TOUTES") || terminees != null
				|| nondebutees != null || encours != null || mes_e_remp != null || mes_e != null || ouvertes != null) {

			if (!recherche.equals("") && categorie.equals("DEFAUT")) {
				List<Article> lstArticleByName = articleManager.findByNameArticle(recherche);
				lstArticleRecherche.addAll(lstArticleByName);
				req.setAttribute("lstArticleRecherche", lstArticleRecherche);
			} else if (categorie != null) {
				if (!categorie.equals("TOUTES") && !categorie.equals("DEFAUT")
						|| !categorie.equals("TOUTES") && !categorie.equals("DEFAUT") && !recherche.equals("")) {
					lstArticleRecherche = articleManager.findArticleByCategorie(categorie);
					req.setAttribute("lstArticleRecherche", lstArticleRecherche);
					if (!recherche.equals("")) {
						List<Article> lstArticleByName = articleManager.findByNameArticle(recherche);
						if (!lstArticleRecherche.isEmpty() && !lstArticleByName.isEmpty()) {
							List<Article> lstArticleByNameAndCategorie = new ArrayList<>();
							for (Article article : lstArticleRecherche) {
								if (article.getNomArticle().toUpperCase().contains(recherche.toUpperCase())) {
									lstArticleByNameAndCategorie.add(article);
								}
							}
							req.setAttribute("lstArticleRecherche", lstArticleByNameAndCategorie);
						}
					} else {
						lstArticleRecherche = articleManager.findArticleByCategorie(categorie);
						req.setAttribute("lstArticleRecherche", lstArticleRecherche);
					}
				} else if (terminees != null || nondebutees != null || encours != null || mes_e_remp != null
						|| mes_e != null || ouvertes != null) {
					lstArticleRecherche.addAll(lstArticleOuvertes);
					req.setAttribute("lstArticleRecherche", lstArticleRecherche);
				} else {
					lstArticleRecherche = articleManager.findAllArticle();
					req.setAttribute("lstArticleRecherche", lstArticleRecherche);

				}
			}

		}
		this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
	}
}
