package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/connect/profil-vendeur")
public class ProfilVendeurControler extends HttpServlet {

	UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String pseudo = req.getParameter("vendeur");
		Utilisateur vendeur = utilisateurManager.findByPseudo(pseudo);
		Utilisateur utilisateurConnecte = (Utilisateur) req.getSession().getAttribute("utilisateur");
		boolean siVendeur = false;
		if (vendeur.getPseudo().equals(utilisateurConnecte.getPseudo())) {
			siVendeur = true;
		}
		
		req.setAttribute("vendeur", vendeur);
		req.setAttribute("siVendeur", siVendeur);

		this.getServletContext().getRequestDispatcher("/jsp/profil-vendeur.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/jsp/modifier-profil.jsp").forward(req, resp);
	}

}
