package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/connexion")
public class ConnexionControler extends HttpServlet {

	
	UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();
	public ConnexionControler() {
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		this.getServletContext()
	    .getRequestDispatcher("/jsp/connexion.jsp")
	    .forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String identifiant = req.getParameter("identifiant");
		String mdp = req.getParameter("password");
		String message = "Erreur lors de la saisie, veuillez recommencer";
		
		Utilisateur utilisateur = utilisateurManager.findByMailOrPseudoAndMdp(identifiant, mdp);
		
		if (utilisateur.getNoUtilisateur() > 0 ) {
				req.getSession().setAttribute("utilisateur", utilisateur);
				resp.sendRedirect(req.getContextPath() + "/connect/mon-profil");
			} else {
				req.setAttribute("message", message);
				this.getServletContext()
				.getRequestDispatcher("/jsp/connexion.jsp")
				.forward(req, resp);
			}	
	}
		
		//req.getSession().setAttribute("user", user);
		//this.getServletContext().getRequestDispatcher("/jsp/mon-profil.jsp").forward(req, resp);
		
		
	}

