	package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;
import java.time.LocalDateTime;

import fr.eni.ecole.Enibay.bll.bo.Article;
import fr.eni.ecole.Enibay.bll.bo.Categorie;
import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ArticleManager;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/connect/nouvelle-vente")
public class NouvelleVenteControler extends HttpServlet {


	ArticleManager articleManager = ManagerFactory.getArticleManager();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext()
	    .getRequestDispatcher("/jsp/nouvelle-vente.jsp")
	    .forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String nomarticle = req.getParameter("nomarticle");
		String description = req.getParameter("description");
		String categorie = req.getParameter("categorie");
		String miseaprix = req.getParameter("miseaprix");
		String datedebut = req.getParameter("datedebut");
		String heuredebut = req.getParameter("heuredebut");
		String datefin = req.getParameter("datefin");
		String heurefin = req.getParameter("heurefin");
		String rue = req.getParameter("rue");
		String codePostal = req.getParameter("codePostal");
		String ville = req.getParameter("ville");
		Utilisateur utilisateur = (Utilisateur) req.getSession().getAttribute("utilisateur");	
		
		String debut = datedebut+"T"+heuredebut;
		String fin = datefin+"T"+heurefin;

		Article article = new Article(nomarticle, description, LocalDateTime.parse(debut), LocalDateTime.parse(fin), Integer.valueOf(miseaprix), Integer.valueOf(miseaprix), Categorie.valueOf(categorie), rue ,codePostal, ville, utilisateur);

		articleManager.saveArticle(article);
		resp.sendRedirect(req.getContextPath() + "/index");
		//this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
		
	}

}
