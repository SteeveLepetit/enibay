<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header>
	<nav>
		<div class="nav-wrapper indigo accent-3">

			<a href="${pageContext.request.contextPath}/index" class="brand-logo">
				<img alt="" src="${pageContext.request.contextPath}/img/enibay_logo.png"></a> 
			<a href="${pageContext.request.contextPath}/index" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>

			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<c:if test="${ utilisateur == null}">
					<li><a href="${pageContext.request.contextPath}/inscription">S'inscrire</a></li>
					<li><a href="${pageContext.request.contextPath}/connexion">Se connecter</a></li>
				</c:if>
				<c:if test="${ utilisateur != null}">
					<li><a href="${pageContext.request.contextPath}/index">Ench�res</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/mon-profil">Mon profil</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/nouvelle-vente">Vendre un article</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/deconnexion">D�connexion</a></li>
				</c:if>
			</ul> 

			<%-- <ul class="sidenav" id="mobile-demo">
				<c:if test="${ utilisateur == null}">
					<li><a href="${pageContext.request.contextPath}/inscription">S'inscrire</a></li>
					<li><a href="${pageContext.request.contextPath}/connexion">Se connecter</a></li>
				</c:if>
				<c:if test="${ utilisateur != null}">
					<li><a href="${pageContext.request.contextPath}/index">Ench�res</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/mon-profil">Mon profil</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/nouvelle-vente">Vendre un article</a></li>
					<li><a
						href="${pageContext.request.contextPath}/connect/deconnexion">D�connexion</a></li>
				</c:if>
			</ul>
		</div>

					<c:if test="${ utilisateur != null}">
					<li><a href="${pageContext.request.contextPath}/index">Ench�res</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/mon-profil">Mon profil</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/nouvelle-vente">Vendre un article</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/deconnexion">D�connexion</a></li>
					</c:if>
		      </ul>
		      
		      <ul class="sidenav" id="mobile-demo">
				    <c:if test="${ utilisateur == null}">
					<li><a href="${pageContext.request.contextPath}/inscription">S'inscrire</a></li>
					<li><a href="${pageContext.request.contextPath}/connexion">Se connecter</a></li>
					</c:if>
					<c:if test="${ utilisateur != null}">
					<li><a href="${pageContext.request.contextPath}/index">Ench�res</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/mon-profil">Mon profil</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/nouvelle-vente">Vendre un article</a></li>
					<li><a href="${pageContext.request.contextPath}/connect/deconnexion">D�connexion</a></li>
					</c:if>
			  </ul>
    	</div> --%>

	</nav>
</header>

<script src="${pageContext.request.contextPath}/js/principal.js"></script>
