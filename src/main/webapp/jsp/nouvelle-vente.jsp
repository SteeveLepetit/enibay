<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<title>Vendre un article</title>
</head>
<body>
	
	<header>
	<%@ include file="menu.jsp" %>
	</header>
	
	<main class="container row">
			<div class="col s12 m10 l8 push-m1 push-l2"> 
				 <div class="section">	
				 
					<form class="formulaire neumorphism" action="nouvelle-vente" method="post" >
						<div class="row">
							<div class="input-field col s12 m6 l6">
								<input id="nomarticle" type="text" name="nomarticle" value="${param.article}" required/>
								<label for="nomarticle">Nom de l'Article :</label>
							</div>
							  <div class="input-field col s12 m6 l6">
							  		<!-- ATTENTION : génère une erreur si non renseigné -->
								  	<select name="categorie" id="categorie" class="browser-default"> 
										<option selected>Veuillez choisir une catégorie</option>
									    <option value="INFORMATIQUE">Informatique</option>
									    <option value="AMEUBLEMENT">Ameublement</option>
									    <option value="VETEMENT">Vêtement</option>
									    <option value="SPORTLOISIRS">Sports et Loisirs</option>
						   			 </select>
	 						</div>
						</div>
						<div class="row">
							<div class="input-field col s12 m12 l12">
								<input id="description" type="text" name="description" value="${param.description}"/>
								<label for="description">Description</label>
							</div>
						</div>	
						<div class="row">
							<div class="input-field col s12 m6 l6">
								<label for="imgVente"></label>
								<input type="file" 	id="imgVente" name="imgVente" accept="image/*" multiple>
							</div>	 
							<div class="input-field col s12 m6 l6">
								<input id="miseaprix" type="number" name="miseaprix" placeholder="150" value="${param.miseaprix}" required/>
								<label for="miseaprix">Mise à prix de l'enchère</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s6 m3 l3">
								<input class="datepicker" type="date" name="datedebut" placeholder="Choisir date de début" required/>
								<label for="datedebut">Date de début de l'enchère</label>
							</div>	
							<div class="input-field col s6 m3 l3">
								<input class="timepicker" type="time" name="heuredebut" placeholder="Choisir heure" required/>
								<label for="heuredebut">Heure de début</label>
							</div>	
							<div class="input-field col s6 m3 l3">
								<input class="datepicker" type="date" name="datefin" placeholder="Choisir date de fin" required/>
								<label for="datefin">Date de fin de l'enchère</label>
							</div>
							<div class="input-field col s6 m3 l3">
								<input class="timepicker" type="time" name="heurefin" placeholder="Choisir heure" required/>
								<label for="heurefin">Heure de fin</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12 m6 l6">
									<input id="rue" type="text" name="rue" value="${sessionScope.utilisateur.rue}" class="validate" required/>
									<label for="rue">Rue</label>
							</div>	
							<div class="input-field col s12 m2 l2">
									<input id="codePostal" type="text" name="codePostal" value="${sessionScope.utilisateur.codePostal}" class="validate" required/>
									<label for="codePostal">Code Postal</label>
							</div>
							<div class="input-field col s12 s4 l4">
									<input id="ville" type="text" name="ville" value="${sessionScope.utilisateur.ville}" class="validate" required/>
									<label for="ville">Ville</label>
							</div>
						</div>
							<span class="erreur">${erreurs.nom} ${erreurs.date} ${erreurs.retrait} ${erreurs.miseAPrix} ${erreurs.categorie}</span><br>
							<input type="submit" value="Enregistrer la vente" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient"/>
						</form>
						
					 </div> 
				</div>
			</main>
	
	
	<footer>
		<%@ include file="footer.jsp" %>
	</footer>


<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
</body>
</html>