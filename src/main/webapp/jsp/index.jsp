<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Enibay</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">

</head>
<body>
<header>
	<%@ include file="menu.jsp" %>
</header>

<main>


	<form style="background-color: white;" action="index" method="post">
		<div class="container row">
			<div class="col s12 m10 l8 push-m1 push-l2"> 
				<div class="section">	
						<label>Filtres : </label>
						<input id="searchbar" type="text" name="search" placeholder="Rechercher un article"/>
							${erreurNom }
							<div class="input-field col s12">
						   	 <select name="categorie" class="browser-default">
						   	 <option value="DEFAUT" selected>Veuillez choisir une catégorie</option>
						      <option value="TOUTES">Toutes catégories</option>
						      <option value="AMEUBLEMENT">Ameublement</option>
						      <option value="INFORMATIQUE">Informatique</option>
						      <option value="VETEMENT">Vêtements</option>
						      <option value="SPORTLOISIRS">Sports & Loisirs</option>
						    <!--   <button type="submit" class="waves-effect waves-light btn-small"><i class="material-icons">search</i> </button> -->
							</select>
						  </div>
		 			</div>
		   		 </div>
		    </div>
	    
	     <div class="container">
	     	<div class="col s12 m10 l8">
		   		<div class="row">
		   		
		   			<div class="row">
	    <c:if test="${ utilisateur != null}">
		   				<div class="col s12 m6 l4">
		    					<ul role="listbox">
		      					<label>
		       					<input id="indeterminate-checkbox-achat" type="radio" name="radio" value="achats" onclick="onClickFilter()" checked />
		        				<span>Achats</span>
		     					</label>
		     					<li>
							      <label>
							        <input id="choix-achats01" class="decalage" type="checkbox" name="ouvertes" value="ouvertes" />
							        <span>Enchères ouvertes</span>
							      </label>
							    </li>
							    <li>
							      <label>
							        <input id="choix-achats02" class="decalage" type="checkbox" name="mes_e" value="mes_e" />
							        <span>Mes enchères en cours</span>
							      </label>
							    </li>
							    <li>
							      <label>
							        <input id="choix-achats03" class="decalage" type="checkbox" name="mes_e_remp" value="mes_e_remp" />
							        <span>Mes enchères remportées</span>
							      </label>
							    </li>
		    					</ul>	
	    				</div>
	
						<div class="col s12 m6 l4">
		    					<ul role="listbox">
		      					<label>
		       					<input id="indeterminate-checkbox2" type="radio" name="radio" value="ventes" styled="unchecked" onclick="onClickFilter()"/>
		        				<span>Mes ventes</span>
		     					</label>
		     					<li>
							      <label>
							        <input id="choix-ventes01" class="decalage" type="checkbox" name="encours" value="encours" styled="unchecked" disabled/>
							        <span>Mes ventes en cours</span>
							      </label>
							    </li>
							    <li>
							      <label>
							        <input id="choix-ventes02" class="decalage" type="checkbox" name="nondebutees" value="nondebutees" styled="unchecked" disabled/>
							        <span>Ventes non débutées</span>
							      </label>
							    </li>
							    <li>
							      <label>
							        <input id="choix-ventes03" class="decalage" type="checkbox" name="terminees" value="terminees" styled="unchecked" disabled/>
							        <span>Ventes terminées</span>
							      </label>
							    </li>
		    					</ul>	
		    				
	    				</div>
	    				</c:if>
	    					<div class="col s12 m10 l8 push-l8 push-m6 push-s4">
	    				   		<input type="submit" value="Rechercher" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient"> 
	    					</div>
	    			</div>
	    		</div>
	     	</div>
		</div>
	</form>	
	
    
    <div class="container">
     <div class="col s12 m10 l8">
	    <div class="row inline-block">
			  <c:forEach items="${ lstArticleRecherche }" var="article">  
				    <div class="col s12 m8 l3 xl3">
				      <div class="card">
				        <div class="card-image">
				          <img src="${pageContext.request.contextPath}/img/profil.webp">
				          <a href="${pageContext.request.contextPath}/connect/details-vente?noArticle=${article.noArticle }" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons indigo accent-3">zoom_in</i></a>
				      	  </div>
				       	 <div class="card-content">
				        	<a href="${pageContext.request.contextPath}/connect/details-vente?noArticle=${article.noArticle }">
							    <h4 class="header">${article.nomArticle }</h4> </a>
								    <label>Prix :</label>
							          <p>${article.miseAPrix } points</p>
							        <label>Fin de l'enchère :</label>
							          <fmt:parseDate value="${article.dateFinEnchere}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
							          <p> <fmt:formatDate pattern="dd/MM/yyyy à HH'h'mm" value="${ parsedDateTime }" /></p>
				        </div>
				        <c:if test="${ utilisateur != null}">
				        	<div class="card-action">
					        	<label>Vendeur :</label>
						        	<a href="${pageContext.request.contextPath}/connect/profil-vendeur?vendeur=${article.utilisateur.pseudo }">${article.utilisateur.pseudo }</a>
					        </div>
					    </c:if>
					     <c:if test="${ utilisateur == null}">
				        	<div class="card-action">
					        	<label>Vendeur :</label> <span>${article.utilisateur.pseudo }</span>
					        </div>
					    </c:if>
				      </div>
				    </div>
			  </c:forEach>  
	     </div>
     </div>
	</div>
</main>   


	<footer>
		<%@ include file="footer.jsp" %>
	</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
            
</body>
</html>