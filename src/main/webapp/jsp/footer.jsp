<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<footer class="page-footer indigo accent-3">

		<div class="container row center-align">
			<p>Suivez Enibay sur les r�seaux !</p>
		</div>
		
		<div class="container row">
				<div class="col s12 offset-m3 m10 offset-l4 l8 push-l1"> 
					  <i class="material-icons medium">rss_feed</i>
					  <i class="material-icons medium">email</i>
					  <i class="material-icons medium">question_answer</i>
			  	</div>
		</div>
</footer>