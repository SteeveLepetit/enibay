<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<title>${ utilisateur.pseudo }</title>
</head>
<body>
	<header>
		<%@ include file="menu.jsp" %>
	</header>
	
<main class= "container">
	<div class="col s12 m7">
   		 <h2 class="header center-align light-blue-text">Mon profil</h2>
    		<div class="card horizontal neumorphism">
		      <div class="card-image">
		        <img src="${pageContext.request.contextPath}/img/peluches/01.gif">
		      </div>
     			<div class="card-stacked">
			        <div class="card-content">
								<label for="pseudo">Pseudo : </label> <p> ${ utilisateur.pseudo } </p> <br/>
								<label for="nom">Nom : </label> <p> ${ utilisateur.nom } </p> <br/>
								<label for="prenom">Prenom : </label> <p> ${ utilisateur.prenom } </p> <br/>
								<label for="email">Email : </label> <p> ${ utilisateur.email } </p> <br/>
								<label for="telephone"> Telephone : </label> <p> ${ utilisateur.telephone } </p> <br/>
								<label for="rue">Adresse : </label> <p> ${ utilisateur.rue } </p>
								<label for="code-postal"></label> <p> ${ utilisateur.codePostal } </p>
								<label for="ville"></label> <p> ${ utilisateur.ville } </p>
			        </div>
        <div class="card-action">
       		<form action="mon-profil" method="post">
          		<input type="submit" value="Modifier mon profil" class="waves-effect waves-light btn-small indigo accent-3 anim-bg-gradient"/>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>

	<footer>
		<%@ include file="footer.jsp" %>
	</footer>
	
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
</body>
</html>