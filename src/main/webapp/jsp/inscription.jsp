<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Page d'inscription</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
</head>
<body>
	<header>
		<%@ include file="menu.jsp" %>
	</header>
	
	
		
				
	<main class="container row">
		<div class="col s12 m10 l8 push-m1 push-l2"> 
			 <div class="section">	
			 
			 
				<form class="formulaire neumorphism" action="inscription" method="post">
					<div class="row">
						<div class="input-field col s12 m6 l6">
								<input id="pseudo" type="text" name="pseudo" value="${param.pseudo}" class="validate"/><br> 
								<span class="erreur">${erreurs.pseudoIncorrect}</span>
								<label for="pseudo">Pseudo</label>
						</div>
						<div class="input-field col s12 m6 l6">
								<input id="email" type="email" name="email" value="${param.email}" class="validate"/> 
								<label for="email">Email</label>
						</div> 
					</div>
					<div class="row">
						<div class="input-field col s12 m6 l6">
								<input id="nom" type="text" name="nom" value="${param.nom}" class="validate"/> 
								<label for="nom">Nom</label>
						</div>
						<div class="input-field col s12 m6 l6">
								<input id="prenom" type="text" name="prenom" size="10" maxlength="50" value="${param.prenom}" class="validate"/>
								<label for="prenom">Prénom</label> 
						</div> 
					</div>	
					<div class="row">
						<div class="input-field col s12 m6 l6">
								<input id="ddn" type="date" name="ddn" value="${param.ddn}" placeholder="" class="validate"/>
								<span class="erreur">${erreurs.date}</span>
								<label for="ddn">Date de naissance</label>
						</div>	 
						<div class="input-field col s12 m6 l6">
								<input id="telephone" type="text" name="telephone" value="${param.telephone}" class="validate"/>
								<label for="telephone">Téléphone</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6 l6">
								<input id="rue" type="text" name="rue" value="${param.rue}" class="validate"/>
								<label for="rue">Rue</label>
						</div>	
						<div class="input-field col s12 m2 l2">
								<input id="codePostal" type="text" name="codePostal" value="${param.codePostal}" class="validate"/>
								<label for="codePostal">Code Postal</label>
						</div>
						<div class="input-field col s12 s4 l4">
								<input id="ville" type="text" name="ville" value="${param.ville}" class="validate"/>
								<label for="ville">Ville</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6 l6 blocMdp">
								<input id="mdp" type="password" name="mdp" size="63" class="validate"/>
								<img class="icon" id="oeil" src="img/eye_closed.png"
									onmousedown="afficherMdp()" onmouseup="cacherMdp()" />
								<label for="mdp">Mot de passe</label>
						</div>	
						<div class="input-field col s12 m6 l6 blocMdp right">
								<input id="mdpConf" type="password" name="mdpConf" class="validate"/>
							<!-- 	<img class="icon" id="oeil" src="img/eye_closed.png"
									onmousedown="afficherMdp()" onmouseup="cacherMdp()" /> -->
								<label for="mdpConf">Confirmation du mdp</label>
						</div> 
					</div>
						<span class="erreur">${erreurs.mdp}</span><br>
						<input type="submit" value="Valider l'inscription !" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient"/>
					</form>
					
				 </div> 
			</div>
		</main>
	
	
	<footer>
		<%@ include file="footer.jsp" %>
	</footer>
	
	
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
</body>
</html>