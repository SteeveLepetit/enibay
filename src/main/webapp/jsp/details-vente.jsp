<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/main.css">

<title>Détails de la vente ${ article.nomArticle }</title>

</head>
<body>

	<header> 
	<%@ include file="menu.jsp" %>
	</header>
		<main class="container">
		<main>
		<c:if test="${ nonCommence }">
		<div class="col s12 m7">
			    <h2 class="header center-align light-blue-text">Détails de la vente</h2>
			   		<div class="card horizontal neumorphism">
			     		<div class="card-image">
			       		<img src="${pageContext.request.contextPath}/img/profil.webp">
			      	</div>
			      <div class="card-stacked">
			      	<div class="card-content">

			      				<label for="description">Description : </label> <p> ${ article.description } </p> <br/>
								<label for="categorie">Catégorie : </label> <p> ${article.categorie } </p> <br/>
								<label for="prix">Mise à prix : </label> <p>${article.miseAPrix } points </p> <br/>
								<fmt:parseDate value="${article.dateDebutEnchere}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
								<label for="debutEnchere"> Début de l'enchère : </label> <p> <fmt:formatDate pattern="dd/MM/yyyy à HH'h'mm" value="${ parsedDateTime }" /> </p> <br/>
								<fmt:parseDate value="${article.dateFinEnchere}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
								<label for="finEnchere"> Fin de l'enchère : </label> <p> <fmt:formatDate pattern="dd/MM/yyyy à HH'h'mm" value="${ parsedDateTime }" /> </p> <br/>
								<label for="adresse">Retrait : </label> <p> ${article.rueRetrait } <br/> ${article.codePostalRetrait } ${ article.villeRetrait }</p>
								<label for="vendeur"> Vendeur :</label> <p> <a href="${pageContext.request.contextPath}/connect/profil-vendeur?vendeur=${article.utilisateur.pseudo }">${article.utilisateur.pseudo }</a> </p>
								
			        </div>
			        <div class="card-action row">
			        </div>
			      </div>
			    </div>
			  </div>
		</c:if>
		<c:if test="${ enCours }">
		<div class="col s12 m7">
			    <h2 class="header center-align light-blue-text">Détails de la vente</h2>
			   		<div class="card horizontal">
			     		<div class="card-image">
			       		<img src="${pageContext.request.contextPath}/img/profil.webp">
			      	</div>
			      <div class="card-stacked">
			      	<div class="card-content">

			      				<label for="description">Description : </label> <p> ${ article.description } </p> <br/>
								<label for="categorie">Catégorie : </label> <p> ${article.categorie } </p> <br/>
								<c:if test="${meilleureOffre.getUtilisateur() != null }">
								<label for="offre">Meilleure offre : </label> <p>${article.prixVente } points par ${meilleureOffre.utilisateur.pseudo} </p> <br/>
								</c:if>
								<label for="prix">Mise à prix : </label> <p>${article.miseAPrix } points </p> <br/>
								<fmt:parseDate value="${article.dateFinEnchere}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
								<label for="finEnchere"> Fin de l'enchère : </label> <p> <fmt:formatDate pattern="dd/MM/yyyy à HH'h'mm" value="${ parsedDateTime }" /> </p> <br/>
								<label for="adresse">Retrait : </label> <p> ${article.rueRetrait } <br/> ${article.codePostalRetrait } ${ article.villeRetrait }</p>
								<label for="vendeur"> Vendeur :</label> <p> <a href="${pageContext.request.contextPath}/connect/profil-vendeur?vendeur=${article.utilisateur.pseudo }">${article.utilisateur.pseudo }</a> </p>
								
			        </div>
			        <div class="card-action row">
			        <form action="${pageContext.request.contextPath}/connect/details-vente?noArticle=${article.noArticle }" method="post" class="col s12 m10 l8" >
							<p>Ma proposition : 
							<input type="number" id="encherir" value="${article.prixVente + 10 }" name="encherir" min="10" >
							<input type="hidden" value="${ article.noArticle }" name="noArticle"> 
							<input type="submit" name="encherirButton" value="Enchérir" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient"/>
							</p>
							<p> ${ erreur }
							</p>
						</form>
			        </div>
			      </div>
			    </div>
			  </div>
			  
		</c:if>
		<c:if test="${termine }">
			<div class="col s12 m7">
				<c:if test="${nomGagnant }">
					<h2 class="header center-align">Vous avez gagné ${ article.nomArticle }</h2>
				</c:if>
				<c:if test="${ !nomGagnant }">
					<h2 class="header center-align">${ meilleureOffre.utilisateur.pseudo} a gagné ${ article.nomArticle }</h2>
				</c:if>
				    
				   		<div class="card horizontal">
				     		<div class="card-image">
				       		<img src="${pageContext.request.contextPath}/img/profil.webp">
				      	</div>
				      <div class="card-stacked">
				      	<div class="card-content">
									<p> ${ article.nomArticle } </p> <br/>
				      				<label for="description">Description : </label> <p> ${ article.description } </p> <br/>
									<label for="offre">Meilleure offre : </label> <p>${article.prixVente } points par ${ meilleureOffre.utilisateur.pseudo}</p> <br/>
									<label for="prix">Mise à prix : </label> <p>${article.miseAPrix } points </p> <br/>
									<label for="adresse">Retrait : </label> <p> ${article.rueRetrait } <br/> ${article.codePostalRetrait } ${ article.villeRetrait }</p>
									<label for="vendeur"> Vendeur :</label> <p> <a href="${pageContext.request.contextPath}/connect/profil-vendeur?vendeur=${article.utilisateur.pseudo }">${article.utilisateur.pseudo }</a> </p>
									<c:if test="${ article.utilisateur.telephone }">
									<label for="telephone"> tel :</label> <p> ${article.utilisateur.telephone } </p>
									</c:if>
				        </div>
				        <div class="card-action row">
				        <c:if test="${ !nomVendeur }">
						<input type="submit" name="retourPage" onclick="history.back()" value="Back" class="waves-effect waves-light btn-large"/>
						</c:if>
						<c:if test="${ nomVendeur }">
						<input type="submit" name="retraitEffectue" value="Retrait effectué" class="waves-effect waves-light btn-large"/>
						</c:if>
				     		
				        </div>
				      </div>
				    </div>
				  </div>
		</c:if>
		
		</main>

	<footer>
		<%@ include file="footer.jsp" %>
	</footer>


	<script src="${pageContext.request.contextPath}/js/materialize.js">
	
	</script>
	<script src="${pageContext.request.contextPath}/js/principal.js">
		
	</script>
	<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
</body>
</html>