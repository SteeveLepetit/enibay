<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<title>Modifier mon profil</title>
</head>
<body>

	<header>
		<%@ include file="menu.jsp" %>
	</header>
	
	
	
	<main class="container row">
		<div class="col s12 m10 l8 push-m1 push-l2"> 
			 <div class="section">	
			 
			 
				<form class="formulaire neumorphism" action="${pageContext.request.contextPath}/connect/modifier-profil" method="post">
					<div class="row">
							<div class="input-field col s12 m12 l4">
									<input id="pseudo" type="text" maxlength="50" name="pseudo" value="${ sessionScope.utilisateur.pseudo }" class="validate"/>
									<label for="pseudo">Pseudo</label>
							</div>
							<div class="input-field col s12 m6 l4">
									<input id="nom" type="text"  maxlength="50" name="nom" value="${ sessionScope.utilisateur.nom }" class="validate"/>
									<label for="nom">Nom</label>
							</div>
							<div class="input-field col s12 m6 l4">
									<input id="prenom" type="text" placeholder="votre nouveau prenom" maxlength="50" name="prenom" value="${ sessionScope.utilisateur.prenom }" class="validate"/>
									<label for="prenom">Prénom</label> 
							</div> 
						</div>	
					<div class="row">
						<div class="input-field col s12 m6 l6">
								<input id="email" type="email" name="email" value="${ sessionScope.utilisateur.email }" class="validate"/>
								<label for="email">Email</label>
						</div> 
						<div class="input-field col s12 m6 l6">
								<input id="telephone" type="text" placeholder="votre nouveau numero de telephone" maxlength="50" name="telephone" value="${ sessionScope.utilisateur.telephone }" class="validate"/>
								<label for="email">Téléphone</label>
						</div> 
					</div>
					<div class="row">
						<div class="input-field col s12 m12 l4">
								<input id="rue" type="text" name="rue" value="${sessionScope.utilisateur.rue }" class="validate"/>
								<label for="rue">Rue</label>
						</div>	
						<div class="input-field col s12 m6 l4">
								<input id="codePostal" type="text" name="codePostal" value="${sessionScope.utilisateur.codePostal }" class="validate"/>
								<label for="codePostal">Code Postal</label>
						</div>
						<div class="input-field col s12 m6 l4">
								<input id="ville" type="text" name="ville" value="${sessionScope.utilisateur.ville }" class="validate"/>
								<label for="ville">Ville</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m12 l12 blocMdp">
								<input id="mdp" type="password" placeholder="votre mot de passe actuel" maxlength="50" name="mdp" class="validate"/>
								<img class="icon" id="oeil" src="${pageContext.request.contextPath}/img/eye_closed.png"
									alt="logo_oeil_ferme" title="logo_oeil-ferme"
									onmousedown="afficherMdp()" onmouseup="cacherMdp()" />
								<label for="mdp">Mot de passe actuel</label>
						</div>	
						<div class="input-field col s12 m6 l6 blocMdp">
								<input id="newmdp" type="password"name="newmdp" placeholder="Votre nouveau mot de passe" class="validate"/>
								<img class="icon" id="oeil" src="${pageContext.request.contextPath}/img/eye_closed.png"
									alt="logo_oeil_ferme" title="logo_oeil-ferme"
									onmousedown="afficherMdp()" onmouseup="cacherMdp()" />
								<label for="newmdp">Nouveau mot de passe</label>
						</div>	
						<div class="input-field col s12 m6 l6 blocMdp">
								<input id="confirmation" type="password" placeholder="Confirmez votre nouveau mot de passe" maxlength="50" name="confirmation" class="validate"/>
								<img class="icon" id="oeil" src="${pageContext.request.contextPath}/img/eye_closed.png"
									alt="logo_oeil_ferme" title="logo_oeil-ferme"
									onmousedown="afficherMdp()" onmouseup="cacherMdp()" />
								<label for="confirmation">Confirmation du nouveau mdp</label>
						</div> 
					</div>
					<div class="row">
						<div class="input-field col s12 m12 l12 blocMdp">
								<input id="credit" type="hidden" placeholder="Confirmez votre nouveau mot de passe" maxlength="50" name="credit" />
								<label for="credit">Credit : ${ sessionScope.utilisateur.credit }</label>
						</div>	
					</div>	
						<span class="erreur">${erreurs.pseudoIncorrect}</span><br>
						<span class="erreur" id="mauvaisMdp" value="${message}">${message}</span><br>
						<span class="erreur">${erreurs.mdp}</span>
						
						<button type="submit" name="action" value="modifier" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient">Enregistrer les modifications</button><br>
  						<br>
  						<button type="submit" name="action" value="supprimer" class="waves-effect waves-light btn-small indigo accent-3 anim-bg-gradient">Supprimer mon profil</button>
					</form>
					
				 </div> 
			</div>
		</main>

	<footer>
		<%@ include file="footer.jsp" %>
	</footer>
	
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>

</body>
</html>