<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<title>Page de connexion</title>
</head>
<body>


	<header>
		<%@ include file="menu.jsp"%>
	</header>

		<main class="container row">
			<div class="col s12 m10 l8 push-m1 push-l2"> 
				<div class="section">	
					<form class="formulaire neumorphism" action="connexion" method="post">
						<div class="input-field col s12 m6">		
							<input id="identifiant" type="text" name="identifiant" value="${param.pseudo}" class="validate"/>
							<label for="identifiant">Votre identifiant (mail ou pseudo)</label> 
						</div>
						<div class="input-field col s12 m6">
							<input id="password" type="password" name="password" value="" class="validate"/><span class="erreur" id="mauvaiseConnexion" value="${message}">${message}</span><br>
							<label for="password">Votre mot de passe</label>
						</div>
						
						<div class="container row">
							<div class="section col s12 m6 l6">
								<input type="submit" value="Se connecter" class="waves-effect waves-light btn-large indigo accent-3 anim-bg-gradient"/><br>
							</div>
							<!-- <div class="section col s12 m6 l6 push-l3">
								<input type="button" name="passwordOublie" value="Mot de passe oublié" class="waves-effect waves-light btn-large indigo accent-3"/>
							</div> -->
						</div>
					</form>
				 </div> 
			</div>
		</main>

	<footer>
		<%@ include file="footer.jsp" %>
	</footer>

</body>
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>

</html>