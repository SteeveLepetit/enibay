<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<title>${ vendeur.pseudo }</title>
</head>
<body>
	<header>
		<%@ include file="menu.jsp" %>
	</header>
	
	
	<main class="container">
		<div class="col s12 m7">
		<c:if test="${ !siVendeur }">
	    <h2 class="header center-align light-blue-text">Profil de ${ vendeur.pseudo }</h2>
	    </c:if>
	    <c:if test="${ siVendeur }">
	    <h2 class="header center-align light-blue-text">Mon profil</h2>
	    </c:if>
	    <div class="card horizontal neumorphism">
	      <div class="card-image">
	        <img src="${pageContext.request.contextPath}/img/licorne/02.gif">
	      </div>
	      <div class="card-stacked">
	        <div class="card-content">
	          <form action="profil-vendeur" method="post">
						<label for="pseudo">Pseudo : </label> <p> ${ vendeur.pseudo } </p><br/>
						<label for="nom">Nom : </label> <p> ${ vendeur.nom } </p><br/>
						<label for="prenom">Prenom : </label> <p> ${ vendeur.prenom } </p><br/>
						<label for="email">Email : </label> <p> ${ vendeur.email } </p><br/>
						<label for="telephone"> Telephone : </label> <p> ${ vendeur.telephone } </p><br/>
						<label for="rue">Adresse : </label> <p> ${ vendeur.rue } </p>
						<label for="code-postal"> </label><p> ${ vendeur.codePostal } </p>
						<label for="ville"> </label><p> ${ vendeur.ville } </p>
					</form>
	        </div>
	        <div class="card-action">
	        <c:if test="${ siVendeur }">
	         <form action="${pageContext.request.contextPath}/connect/profil-vendeur" method="post">
	          <input type="submit" value="Modifier mon profil" class="waves-effect waves-light btn-small indigo accent-3 anim-bg-gradient"/>
	        </form>
	        </c:if>
	        </div>
	      </div>
	    </div>
	  </div>
	</main>

	<footer>
		<%@ include file="footer.jsp" %>
	</footer>
	
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
</body>
</html>