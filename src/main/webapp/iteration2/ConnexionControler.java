package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;

import fr.eni.ecole.Enibay.bll.bo.Utilisateur;
import fr.eni.ecole.Enibay.bll.managers.ManagerFactory;
import fr.eni.ecole.Enibay.bll.managers.UtilisateurManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/connexion")
public class ConnexionControler extends HttpServlet {

	
	UtilisateurManager utilisateurManager = ManagerFactory.getUtilisateurManager();
	public ConnexionControler() {
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		this.getServletContext()
	    .getRequestDispatcher("/jsp/connexion.jsp")
	    .forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		var seSouvenirDeMoi = false;
		
		var tab = req.getCookies();
		if(tab != null) {
			for(var item : tab)
			{
				if(item.getName().equals("seSouvenirDeMoi"))
				{
					seSouvenirDeMoi = item.getValue().equals("1")?true:false;
				}
			}
		}
		
		String identifiant = req.getParameter("identifiant");
		String mdp = req.getParameter("password");
		String message = "Erreur lors de la saisie, veuillez recommencer";
		
		Utilisateur utilisateur = utilisateurManager.findByMailOrPseudoAndMdp(identifiant, mdp);
		
		if (utilisateur.getNoUtilisateur() > 0 || seSouvenirDeMoi == true) {
			if(seSouvenirDeMoi == true 
					|| (utilisateur.getPseudo().equals(req.getParameter("identifiant")) 
							&& utilisateur.getMdp().equals(req.getParameter("password")))) {
				if("SeSouvenirDeMoi".equals(req.getParameter("seSouvenir")))
				{
					Cookie cookie = new Cookie("seSouvenirDeMoi", "1");
					cookie.setMaxAge(60 * 60 * 24 * 30);
					resp.addCookie(cookie);
				}
				
				req.getSession().setAttribute("utilisateur", utilisateur);
				resp.sendRedirect(req.getContextPath() + "/connect/mon-profil");
				HttpSession session = req.getSession(true);
				session.setMaxInactiveInterval(5*60);
			} else {
				req.setAttribute("message", message);
				this.getServletContext()
				.getRequestDispatcher("/jsp/connexion.jsp")
				.forward(req, resp);
			}	
	}
		
		//req.getSession().setAttribute("user", user);
		//this.getServletContext().getRequestDispatcher("/jsp/mon-profil.jsp").forward(req, resp);
		
		
	}
	
	

}
