<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/materialize.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">

<title>Page de connexion</title>
</head>
<body>


	<header>
		<%@ include file="menu.jsp"%>
	</header>

		<main class="container row">
			<div class="col s12 m10 l8 push-m1 push-l2"> 
				<div class="section">	
					<form class="formulaire" action="connexion" method="post">
						<div class="input-field col s6">		
							<input id="identifiant" type="text" name="identifiant" value="${param.pseudo}" class="validate"/>
							<label for="identifiant">Votre identifiant (mail ou pseudo)</label> 
						</div>
						<div class="input-field col s6">
							<input id="password" type="password" name="password" value="" class="validate"/><span class="erreur" id="mauvaiseConnexion" value="${message}">${message}</span><br>
							<label for="password">Votre mot de passe</label>
						</div>
						<label>
						   	<input id="SeSouvenirDeMoi" class="decalage" type="checkbox" name="seSouvenir" value="SeSouvenirDeMoi" />
						  	<span>Se souvenir de moi</span>
						</label>
						
						<div class="container row">
							<div class="section col s12 m6 l6 push-l5">
								<input type="submit" value="Se connecter" class="waves-effect waves-light btn-large indigo accent-1"/><br>
							</div>
							<div class="section col s12 m6 l6 push-l3">
							<input type="button" name="passwordOublie" value="Mot de passe oubli�" class="waves-effect waves-light btn-large indigo accent-1"/>
							</div>
						</div>
					</form>
				 </div> 
			</div>
		</main>

	<footer class="page-footer indigo accent-3"></footer>

</body>
<script src="${pageContext.request.contextPath}/js/materialize.js"> </script>
<script src="${pageContext.request.contextPath}/js/principal.js"> </script>
</html>