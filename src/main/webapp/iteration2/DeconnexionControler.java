package fr.eni.ecole.Enibay.controlers;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/connect/deconnexion")
public class DeconnexionControler extends HttpServlet {

	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().invalidate();
		Cookie cookie = new Cookie("seSouvenirDeMoi", "1");
		cookie.setMaxAge(0);
		resp.addCookie(cookie);
		this.getServletContext()
		.getRequestDispatcher("/jsp/index.jsp")
		.forward(req, resp);
	}


	
	
	
}
