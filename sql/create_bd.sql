-- Script de création de la base de données ENCHERES
--   type :      SQL Server 2012
--

Create DATABASE Enibay;

Use Enibay;

CREATE TABLE CATEGORIES (
    noCategorie   INTEGER PRIMARY KEY NOT NULL,
    libelle        VARCHAR(30) NOT NULL
)

CREATE TABLE UTILISATEURS (
    noUtilisateur   INTEGER IDENTITY(1,1) PRIMARY KEY NOT NULL,
    pseudo           VARCHAR(30) UNIQUE NOT NULL,
    nom              VARCHAR(30) NOT NULL,
    prenom           VARCHAR(30) NOT NULL,
	ddn				DATE NOT NULL,
    email            VARCHAR(50) UNIQUE NOT NULL,
    telephone        VARCHAR(15),
    rue              VARCHAR(30) NOT NULL,
    codePostal      VARCHAR(10) NOT NULL,
    ville            VARCHAR(50) NOT NULL,
    mdp     VARCHAR(30) NOT NULL,
    credit           INTEGER NOT NULL,
    administrateur   bit CONSTRAINT df_admin DEFAULT 0 NOT NULL,
	dateInscription	DATE NOT NULL
)

CREATE TABLE ARTICLES (
    noArticle                    INTEGER IDENTITY(1,1) PRIMARY KEY NOT NULL,
    nomArticle                   VARCHAR(30) NOT NULL,
    description                   VARCHAR(300) NOT NULL,
	dateDebutEncheres           DATETIME NOT NULL,
    dateFinEncheres             DATETIME NOT NULL,
    miseAPrix                  INTEGER,
    prixVente                    INTEGER,
    noUtilisateur                INTEGER,
    noCategorie                  INTEGER,
	rueRetrait              VARCHAR(30) NOT NULL,
    codePostalRetrait      VARCHAR(15) NOT NULL,
    villeRetrait            VARCHAR(30) NOT NULL,
	etatVente VARCHAR(30) NOT NULL,

	CONSTRAINT ck_superieur_debut CHECK (DATEDIFF (second,dateDebutEncheres, dateFinEncheres) > 0)
)

CREATE TABLE ENCHERES(	
	noEnchere  INTEGER IDENTITY(1,1) PRIMARY KEY NOT NULL,
	dateEnchere datetime NOT NULL,
	montantEnchere INTEGER NOT NULL,
	noArticle INTEGER NOT NULL,
	noUtilisateur INTEGER
 )

 

ALTER TABLE ENCHERES
    ADD CONSTRAINT encheres_utilisateur_fk FOREIGN KEY ( noUtilisateur ) REFERENCES UTILISATEURS ( noUtilisateur )
ON DELETE SET NULL
    ON UPDATE no action 

ALTER TABLE ENCHERES
    ADD CONSTRAINT encheres_no_article_fk FOREIGN KEY ( noArticle ) REFERENCES ARTICLES ( noArticle )
ON DELETE NO ACTION 
    ON UPDATE no action 
	

ALTER TABLE ARTICLES
    ADD CONSTRAINT articles_vendus_categories_fk FOREIGN KEY ( noCategorie )
        REFERENCES categories ( noCategorie )
ON DELETE SET NULL
    ON UPDATE CASCADE

ALTER TABLE ARTICLES
    ADD CONSTRAINT ventes_utilisateur_fk FOREIGN KEY ( noUtilisateur )
        REFERENCES utilisateurs ( noUtilisateur )
ON DELETE SET NULL
    ON UPDATE CASCADE


SELECT * FROM UTILISATEURS;

SELECT * FROM ARTICLES;

DELETE FROM UTILISATEURS;

SELECT * FROM ARTICLES A INNER JOIN UTILISATEURS U ON A.noUtilisateur = U.noUtilisateur
	JOIN CATEGORIES C ON A.noCategorie = C.noCategorie;


INSERT INTO UTILISATEURS (pseudo,nom,prenom,ddn,email,telephone,rue,codePostal,ville,mdp,credit,dateInscription)
VALUES ('toto','tata','titi','1990-06-24','toto@titi.fr','06000000','le triangle','35000','Rennes','123','100',GETDATE()); 

INSERT INTO UTILISATEURS (pseudo,nom,prenom,ddn,email,telephone,rue,codePostal,ville,mdp,credit,dateInscription)
VALUES ('void35','void','Licornus','1990-06-24','void@titi.fr','06000001','des fleurs','35000','Rennes','123','100',GETDATE()); 



INSERT INTO ARTICLES (nomArticle,description,dateDebutEncheres,dateFinEncheres,miseAPrix,
					  prixVente,noUtilisateur,noCategorie,rueRetrait,codePostalRetrait,villeRetrait,etatVente)
			VALUES ('chien', 'void est a vendre','2022-05-10','2022-05-22','32','64','5','1','rue des fleurs',
					'35000','Rennes','NON_COMMENCE')

INSERT INTO ARTICLES (nomArticle,description,dateDebutEncheres,dateFinEncheres,miseAPrix,
					  prixVente,noUtilisateur,noCategorie,rueRetrait,codePostalRetrait,villeRetrait,etatVente)
			VALUES ('peluche', 'peluche description','2022-05-10','2022-05-22','32','64','4','4','rue des palmiers',
					'35000','Rennes','EN_COURS')



 SELECT * FROM CATEGORIES;
 INSERT INTO CATEGORIES (noCategorie,libelle) VALUES ('1','AMEUBLEMENT');
 INSERT INTO CATEGORIES (noCategorie,libelle) VALUES ('2','INFORMATIQUE');
 INSERT INTO CATEGORIES (noCategorie,libelle) VALUES ('3','VETEMENT');
 INSERT INTO CATEGORIES (noCategorie,libelle) VALUES ('4','SPORTLOISIRS');

 DROP DATABASE Enibay